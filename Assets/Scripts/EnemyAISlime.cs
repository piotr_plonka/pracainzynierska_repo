using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAISlime : MonoBehaviour, IEnemy
{
    public int Experience { get; set; }
    public int Money { get; set; }
    public Spawner Spawner { get; set; }
    [Header("EnemyStats")]
    public int currentHealth;
    public int maxHealth;
    public float speed;
    public float checkRadius;
    public float attackRadius;
    [Header("EnemySettings")]
    public bool shouldRotate;
    public LayerMask whatIsPlayer;
    public Vector2 dir;
    private Transform target;
    private Rigidbody2D rb;
    private Animator anim;
    private Vector2 movement;
    private bool isInChaseRange;
    private bool isInAttackRange;
    [Header("DropTable")]
    public GameObject dropThisItem;
    public ItemObject newItem;
    public List<ItemObject> itemsToDrop;
    public int[] table =
    {
        60,
        25,
        10,
        5
    };
    private int total;
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        target = GameObject.FindWithTag("Player").transform;
        Experience = 250;
        Money = 2;
    }
    private void Update()
    {
        anim.SetBool("isRunning", isInChaseRange);
        isInChaseRange = Physics2D.OverlapCircle(transform.position, checkRadius, whatIsPlayer);
        isInAttackRange = Physics2D.OverlapCircle(transform.position, attackRadius, whatIsPlayer);
        dir = target.position - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        dir.Normalize();
        movement = dir;
        if (shouldRotate)
        {
            anim.SetFloat("X", dir.x);
            anim.SetFloat("Y", dir.y);
        }
    }
    private void FixedUpdate()
    {
        if (isInChaseRange && !isInAttackRange)
        {
            MoveCharacter(movement);
        }
        if (isInAttackRange)
        {
            rb.velocity = Vector2.zero;
        }
    }
    private void MoveCharacter(Vector2 dir)
    {
        rb.MovePosition((Vector2)transform.position + (dir * speed * Time.deltaTime));
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "WeaponHitBox")
        {
            Vector2 difference = transform.position - other.transform.position;
            transform.position = new Vector2(transform.position.x + difference.x, transform.position.y + difference.y);
        }
    }
    public void HurtEnemy(int damageToGive)
    {
        currentHealth -= damageToGive;
        if (currentHealth <= 0)
        {
            Die();
        }
    }
    public void Die()
	{
        GenerateRandomItem();
        this.Spawner.Respawn();
        Destroy(gameObject);
        CombatEvents.EnemyDied(this);
    }
    public void GenerateRandomItem()
    {
        int randomNumber;
        foreach (var item in table)
		{
            total += item;
		}
        randomNumber = Random.Range(0, total);
        for (int i = 0; i < table.Length; i++)
        {
            if (randomNumber <= table[i])
            {
                newItem = itemsToDrop[i];
                DropItem(newItem);
                return;
            }
            else
            {
                randomNumber -= table[i];
            }
        }

    }
    public void DropItem(ItemObject _item)
    {
        Debug.Log(_item.data.Name);
        dropThisItem.transform.GetComponent<GroundItem>().item = _item;
        Instantiate(dropThisItem, transform.position, Quaternion.identity);
    }
}
