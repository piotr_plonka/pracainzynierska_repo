using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerHealthManager : MonoBehaviour
{

    public int currentHealth;
    public int maxHealth;
    public GameObject deathscreenUI;
    private bool flashActive;
    [SerializeField]
    private float flashLength = 0f;
    private float flashCounter = 0f;
    private SpriteRenderer playerSpriteRenderer;
    public TextMeshProUGUI healthDisplay;
    public Slider slider;
    public GameObject GUI;

    private void Start()
    {
        playerSpriteRenderer = GetComponent<SpriteRenderer>();

    }

    void Update()
    {
        SetHealth();
        SetMaxHealth();
        DisplayHealth();
        Flash();
    }

    public void HurtPlayer(int damageToGive)
    {
        currentHealth -= damageToGive;
        flashActive = true;
        flashCounter = flashLength;
        if (currentHealth <= 0)
        {
            GUI.SetActive(false);
            gameObject.SetActive(false);
            deathscreenUI.SetActive(true);
        }
    }
    public void SetMaxHealth()
    {
        slider.maxValue = maxHealth;
        slider.value = currentHealth;
    }
    public void SetHealth()
    {
        slider.value = currentHealth;
    }
    public void DisplayHealth()
    {
        healthDisplay.text = currentHealth.ToString();
    }
    public void Flash()
	{
        if (flashActive)
        {
            if (flashCounter > flashLength * 0.99f)
                playerSpriteRenderer.color = new Color(playerSpriteRenderer.color.r, playerSpriteRenderer.color.g, playerSpriteRenderer.color.b, 0f);
            else if (flashCounter > flashLength * 0.82f)
                playerSpriteRenderer.color = new Color(playerSpriteRenderer.color.r, playerSpriteRenderer.color.g, playerSpriteRenderer.color.b, 1f);
            else if (flashCounter > flashLength * 0.66f)
                playerSpriteRenderer.color = new Color(playerSpriteRenderer.color.r, playerSpriteRenderer.color.g, playerSpriteRenderer.color.b, 0f);
            else if (flashCounter > flashLength * 0.49f)
                playerSpriteRenderer.color = new Color(playerSpriteRenderer.color.r, playerSpriteRenderer.color.g, playerSpriteRenderer.color.b, 1f);
            else if (flashCounter > flashLength * 0.33f)
                playerSpriteRenderer.color = new Color(playerSpriteRenderer.color.r, playerSpriteRenderer.color.g, playerSpriteRenderer.color.b, 0f);
            else if (flashCounter > flashLength * 0.16f)
                playerSpriteRenderer.color = new Color(playerSpriteRenderer.color.r, playerSpriteRenderer.color.g, playerSpriteRenderer.color.b, 1f);
            else if (flashCounter > 0f)
                playerSpriteRenderer.color = new Color(playerSpriteRenderer.color.r, playerSpriteRenderer.color.g, playerSpriteRenderer.color.b, 0f);
            else
            {
                playerSpriteRenderer.color = new Color(playerSpriteRenderer.color.r, playerSpriteRenderer.color.g, playerSpriteRenderer.color.b, 1f);
                flashActive = false;
            }
            flashCounter -= Time.deltaTime;
        }
    }
}
