using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLevel : MonoBehaviour
{
    public int Money { get; set; }
    public int Level { get; set; }
    public int CurrentExperience { get; set; }
    public int RequiredExperience { get { return Level * 25; } }

    void Start()
    {
        CombatEvents.OnEnemyDeath += EnemyToMoney;
        Money = 0;
        CombatEvents.OnEnemyDeath += EnemyToExperience;
        Level = 1;
    }

    public void EnemyToExperience(IEnemy enemy)
    {
        GrantExperience(enemy.Experience);
    }

    public void GrantExperience(int amount)
    {
        CurrentExperience += amount;
        while (CurrentExperience >= RequiredExperience)
        {
            CurrentExperience -= RequiredExperience;
            Level++;
        }
    }
    public void EnemyToMoney(IEnemy enemy)
    {
        GrantMoney(enemy.Money);
    }
    public void GrantMoney(int amount)
    {
        Money += amount;
    }
}