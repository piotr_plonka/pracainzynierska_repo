using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MainMenu : MonoBehaviour
{
	public GameObject settingsPanel;
	public void PlayGame()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}
	public void SettingsPanelOn()
	{
		settingsPanel.SetActive(true);
	}
	public void QuitGame()
	{
		Application.Quit();
	}
}
