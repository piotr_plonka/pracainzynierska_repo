using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using XNode;
using UnityEngine.SceneManagement;
using TMPro;
public class NodeParser : MonoBehaviour
{
    public DialogueGraph graph;
	Coroutine _parser;
	public TextMeshProUGUI speaker;
	public TextMeshProUGUI dialogue;
	public Image speakerImage;
	public string sScene;
	[SerializeField]
	private float typeWriterSpeed = 15f;

	private void Start()
	{
		foreach (BaseNode b in graph.nodes)
		{
			if(b.GetString() == "Start")
			{
				graph.current = b;
				break;
			}
		}
		_parser = StartCoroutine(ParseNode());
	}

	IEnumerator ParseNode()
	{
		BaseNode b = graph.current;
		string data = b.GetString();
		string[] dataParts = data.Split('/');
		if(dataParts[0] == "Start")
		{
			NextNode("exit");
		}
		if (dataParts[0] == "DialogueNode")
		{
			speaker.text = dataParts[1];
			StartCoroutine(TypeText(dataParts[2], dialogue));
			speakerImage.sprite = b.GetSprite();
			if (speakerImage.sprite == null) speakerImage.enabled = false;
			else speakerImage.enabled = true;
			yield return new WaitUntil(() => Input.GetMouseButtonDown(0));
			yield return new WaitUntil(() => Input.GetMouseButtonUp(0));
			if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonUp(0))
			{
				StopCoroutine(ParseNode());
				StopCoroutine(TypeText("", dialogue));
			}
			NextNode("exit");
		}
		if(dataParts[0] == "End")
		{
			sScene = dataParts[1];
			SceneManager.LoadScene(sScene);
		}
		
	}

	public void NextNode(string fieldName)
	{
		if (_parser != null)
		{
			StopCoroutine(_parser);
			_parser = null;
		}
		foreach (NodePort p in graph.current.Ports)
		{
			if(p.fieldName == fieldName)
			{
				graph.current = p.Connection.node as BaseNode;
				break;
			}
			
		}
		_parser = StartCoroutine(ParseNode());
	}

	public IEnumerator TypeText(string textToType, TextMeshProUGUI textLabel)
	{
		textLabel.text = string.Empty;
		float t = 0;
		int charIndex = 0;
		while (charIndex < textToType.Length)
		{
			t += Time.deltaTime * typeWriterSpeed;
			charIndex = Mathf.FloorToInt(t);
			charIndex = Mathf.Clamp(charIndex, 0, textToType.Length);
			textLabel.text = textToType.Substring(0, charIndex);
			yield return null;
		}
		textLabel.text = textToType;
	}
}
