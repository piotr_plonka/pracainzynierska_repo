using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
public class SettingsMenu : MonoBehaviour
{
	public GameObject settingsPanel;
	public AudioMixer audioMixer;
	public Dropdown resolutionDropdown;
	Resolution[] resolutions;

	void Start()
	{
		resolutions = Screen.resolutions;
		resolutionDropdown.ClearOptions();
		List<string> resolutionOptions = new List<string>();
		int currentResolutionIndex = 0;
		for (int i = 0; i < resolutions.Length; i++)
		{
			string resolutionOption = resolutions[i].width + "x" + resolutions[i].height;
			resolutionOptions.Add(resolutionOption);
			if(resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
			{
				currentResolutionIndex = i;
			}
		}
		resolutionDropdown.AddOptions(resolutionOptions);
		resolutionDropdown.value = currentResolutionIndex;
		resolutionDropdown.RefreshShownValue();
	}
	public void SetVolumne(float volumne)
	{
		audioMixer.SetFloat("volumne", volumne);
	}
	public void SetQuality(int qualityIndex)
	{
		QualitySettings.SetQualityLevel(qualityIndex);
	}
	public void SetResolution (int resolutionIndex)
	{
		Resolution resolution = resolutions[resolutionIndex];
		Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
	}
	public void SetFullScreen (bool isFullscreen)
	{
		Screen.fullScreen = isFullscreen;
	}
	public void SettingsMenuOff()
	{
		settingsPanel.SetActive(false);
	}
}
