using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Player : MonoBehaviour
{
    public MouseItem mouseItem = new MouseItem();
    public static bool gameIsPaused = false;
    [Header("Playersettings")]
    public Rigidbody2D rb;
    public Animator animator;
    public float attackTime = 0.5f;
    public float attackCounter = 0.5f;
    private bool isAttacking;
    public float moveSpeed = 100f;
    public PlayerLevel playerLevel { get; set; }
    [Header("Health")]
    public int currentHealth;
    public int maxHealth;
    [Header("Health settings")]
    public GameObject deathscreenUI;
    private bool flashActive;
    [SerializeField]
    private float flashLength = 0f;
    private float flashCounter = 0f;
    private SpriteRenderer playerSpriteRenderer;
    public Text healthDisplay;
    public Slider slider;
    public GameObject GUI;
    [Header("Inventory/equipment")]
    public InventoryObject inventory;
    public InventoryObject equipment;
    [Header("Stats")]
    public Attribute[] attributes;
    public Text levelText;
    public Text expText;
    public Text moneyText;
    [Header("Inventory show/hide")]
    [SerializeField]
    public Canvas inventoryCanvas;

    private void Start()
	{
        playerLevel = GetComponent<PlayerLevel>();
        playerSpriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        if (!PlayerPrefs.HasKey("SaveState"))
        {
            PlayerPrefs.SetInt("SaveState", 0);
        }
        for (int i = 0; i < attributes.Length; i++)
        {
            attributes[i].SetParent(this);
        }
        for (int i = 0; i < equipment.GetSlots.Length; i++)
        {
            equipment.GetSlots[i].OnBeforeUpdate += OnBeforeSlotUpdate;
            equipment.GetSlots[i].OnAfterUpdate += OnAfterSlotUpdate;
        }
    }
	private void Update()
    {
        SetHealth();
        SetMaxHealth();
        DisplayHealth();
        Flash();
        Movement();
        IsAttacking();
        UpdateUILevel();
        UpdateUIMoney();
    }
    public void IsAttacking() 
    {
        if (isAttacking)
        {
            moveSpeed = 0;
            attackCounter -= Time.deltaTime;
            if (attackCounter <= 0)
            {
                moveSpeed = 100f;
                animator.SetBool("isAttacking", false);
                isAttacking = false;
            }
        }
    }
    public void Movement()
	{
        animator.SetFloat("moveX", rb.velocity.x);
        animator.SetFloat("moveY", rb.velocity.y);
        if (Input.GetAxisRaw("Horizontal") == 1 || Input.GetAxisRaw("Horizontal") == -1 || Input.GetAxisRaw("Vertical") == 1 || Input.GetAxisRaw("Vertical") == -1)
        {
            animator.SetFloat("lastMoveX", Input.GetAxisRaw("Horizontal"));
            animator.SetFloat("lastMoveY", Input.GetAxisRaw("Vertical"));
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("Inventory & Equipment Saved");
            equipment.Save();
            inventory.Save();
        }
        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            Debug.Log("Inventory & Equipment Loaded");
            equipment.Load();
            inventory.Load();
        }
        if (Input.GetKeyDown(KeyCode.Tab) || Input.GetKeyDown(KeyCode.E))
        {
            if (gameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
        if (Input.GetMouseButton(0))
        {
            if (isAttacking != true)
            {
                moveSpeed = 0;
                attackCounter = attackTime;
                isAttacking = true;
                animator.SetBool("isAttacking", true);
            }
        }
    }
    void FixedUpdate()
    {
        rb.velocity = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")) * moveSpeed * Time.deltaTime;
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        var groundItem = other.GetComponent<GroundItem>();
        if (groundItem)
        {
            Item _item = new Item(groundItem.item);
            if (inventory.AddItem(_item, 1))
            {
                Destroy(other.gameObject);
            }
        }
    }
    public void AttributeModified(Attribute attribute)
    {
        Debug.Log(string.Concat(attribute.type, " was updated! Value is now ", attribute.value.ModifiedValue));
    }
    public void OnBeforeSlotUpdate(InventorySlot _slot)
    {
        if (_slot.ItemObject == null)
            return;
        switch (_slot.parent.inventory.type)
        {
            case InterfaceType.Inventory:
                break;
            case InterfaceType.Equipment:
                print(string.Concat("Removed ", _slot.ItemObject, " on ", _slot.parent.inventory.type, ", Allowed Items: ", string.Join(", ", _slot.AllowedItems)));

                for (int i = 0; i < _slot.item.buffs.Length; i++)
                {
                    for (int j = 0; j < attributes.Length; j++)
                    {
                        if (attributes[j].type == _slot.item.buffs[i].attribute)
                            attributes[j].value.RemoveModifier(_slot.item.buffs[i]);
                    }
                }

                break;
            case InterfaceType.Shop:
                break;
            default:
                break;
        }
    }
    public void OnAfterSlotUpdate(InventorySlot _slot)
    {
        if (_slot.ItemObject == null)
            return;
        switch (_slot.parent.inventory.type)
        {
            case InterfaceType.Inventory:
                break;
            case InterfaceType.Equipment:
                print(string.Concat("Placed ", _slot.ItemObject, " on ", _slot.parent.inventory.type, ", Allowed Items: ", string.Join(", ", _slot.AllowedItems)));

                for (int i = 0; i < _slot.item.buffs.Length; i++)
                {
                    for (int j = 0; j < attributes.Length; j++)
                    {
                        if (attributes[j].type == _slot.item.buffs[i].attribute)
                            attributes[j].value.AddModifier(_slot.item.buffs[i]);
                    }
                }

                break;
            case InterfaceType.Shop:
                print(string.Concat("Placed ", _slot.ItemObject, " on ", _slot.parent.inventory.type, ", Allowed Items: ", string.Join(", ", _slot.AllowedItems)));

                for (int i = 0; i < _slot.item.buffs.Length; i++)
                {
                    for (int j = 0; j < attributes.Length; j++)
                    {
                        if (attributes[j].type == _slot.item.buffs[i].attribute)
                            attributes[j].value.AddModifier(_slot.item.buffs[i]);
                    }
                }
                break;
            default:
                break;
        }
    }
    public void TakeDamage(int amount)
    {
        currentHealth -= amount;
        flashActive = true;
        flashCounter = flashLength;
        if (currentHealth <= 0)
        {
            GUI.SetActive(false);
            gameObject.SetActive(false);
            deathscreenUI.SetActive(true);
        }
    }
    public void Resume()
    {
        inventoryCanvas.enabled = false;
        Time.timeScale = 1f;
        gameIsPaused = false;
    }
    void Pause()
    {
        inventoryCanvas.enabled = true;
        Time.timeScale = 0f;
        gameIsPaused = true;
    }
    private void OnApplicationQuit()
    {
        inventory.Clear();
        equipment.Clear();
    }
    public void SetMaxHealth()
    {
        slider.maxValue = maxHealth;
        slider.value = currentHealth;
    }
    public void SetHealth()
    {
        slider.value = currentHealth;
    }
    public void DisplayHealth()
    {
        healthDisplay.text = currentHealth.ToString();
    }
    public void Flash()
    {
        if (flashActive)
        {
            if (flashCounter > flashLength * 0.99f)
                playerSpriteRenderer.color = new Color(playerSpriteRenderer.color.r, playerSpriteRenderer.color.g, playerSpriteRenderer.color.b, 0f);
            else if (flashCounter > flashLength * 0.82f)
                playerSpriteRenderer.color = new Color(playerSpriteRenderer.color.r, playerSpriteRenderer.color.g, playerSpriteRenderer.color.b, 1f);
            else if (flashCounter > flashLength * 0.66f)
                playerSpriteRenderer.color = new Color(playerSpriteRenderer.color.r, playerSpriteRenderer.color.g, playerSpriteRenderer.color.b, 0f);
            else if (flashCounter > flashLength * 0.49f)
                playerSpriteRenderer.color = new Color(playerSpriteRenderer.color.r, playerSpriteRenderer.color.g, playerSpriteRenderer.color.b, 1f);
            else if (flashCounter > flashLength * 0.33f)
                playerSpriteRenderer.color = new Color(playerSpriteRenderer.color.r, playerSpriteRenderer.color.g, playerSpriteRenderer.color.b, 0f);
            else if (flashCounter > flashLength * 0.16f)
                playerSpriteRenderer.color = new Color(playerSpriteRenderer.color.r, playerSpriteRenderer.color.g, playerSpriteRenderer.color.b, 1f);
            else if (flashCounter > 0f)
                playerSpriteRenderer.color = new Color(playerSpriteRenderer.color.r, playerSpriteRenderer.color.g, playerSpriteRenderer.color.b, 0f);
            else
            {
                playerSpriteRenderer.color = new Color(playerSpriteRenderer.color.r, playerSpriteRenderer.color.g, playerSpriteRenderer.color.b, 1f);
                flashActive = false;
            }
            flashCounter -= Time.deltaTime;
        }
    }
    public void UpdateUILevel()
	{
        this.levelText.text = "Level: " + playerLevel.Level.ToString();
        this.expText.text = "Do�wiadczenie: " + playerLevel.CurrentExperience.ToString();
        
    }
    public void UpdateUIMoney()
	{
        this.moneyText.text = "Z�oto: " + playerLevel.Money.ToString();
    }
    public class MouseItem
    {
        public GameObject obj;
        public InventorySlot item;
        public InventorySlot hoverItem;
        public GameObject hoverObj;
    }
}
[System.Serializable]
public class Attribute
{
    [System.NonSerialized] public Player parent;
    public Attributes type;
    public ModifiableInt value;

    public void SetParent(Player _parent)
    {
        parent = _parent;
        value = new ModifiableInt(AttributeModified);
    }

    public void AttributeModified()
    {
        parent.AttributeModified(this);
    }
}