using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using XNode;
using TMPro;

public class QuestNodeParser : MonoBehaviour
{
	
	Coroutine _parser;
	[Header("Graph")]
	public DialogueGraph[] graphs;
	public DialogueGraph currentGraph;
	[Header("Typewriter speed")]
	[SerializeField]
	private float typeWriterSpeed = 15f;
	[Header("UI")]
	public TextMeshProUGUI speaker;
	public TextMeshProUGUI dialogue;
	public Canvas questCanvas;

	[Header("Item For Quest")]
	public ItemObject questItem;

	public void StartDialogue()
	{
		
		
		questCanvas.enabled = true;
		foreach (BaseNode b in currentGraph.nodes)
		{
			if (b.GetString() == "Start")
			{
				currentGraph.current = b;
				break;
			}
		}
		_parser = StartCoroutine(ParseNode());
	}
	IEnumerator ParseNode()
	{
		BaseNode b = currentGraph.current;
		string data = b.GetString();
		string[] dataParts = data.Split('/');
		if (dataParts[0] == "Start")
		{
			NextNode("exit");
		}
		if (dataParts[0] == "QuestDialogueNode")
		{
			speaker.text = dataParts[1];
			StartCoroutine(TypeText(dataParts[2], dialogue));
			yield return new WaitUntil(() => Input.GetMouseButtonDown(0));
			yield return new WaitUntil(() => Input.GetMouseButtonUp(0));
			if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonUp(0))
			{
				StopAllCoroutines();
			}
			NextNode("exit");
		}
		if(dataParts[0] == "QuestItemNode")
		{
			speaker.text = dataParts[1];
			StartCoroutine(TypeText(dataParts[2], dialogue));
			questItem = b.GetItemObject();
			yield return new WaitUntil(() => Input.GetMouseButtonDown(0));
			yield return new WaitUntil(() => Input.GetMouseButtonUp(0));
			if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonUp(0))
			{
				StopAllCoroutines();
			}
			NextNode("exit");
		}
		if (dataParts[0] == "End")
		{
			speaker.text = dataParts[1];
			StartCoroutine(TypeText(dataParts[2], dialogue));
			yield return new WaitUntil(() => Input.GetMouseButtonDown(0));
			yield return new WaitUntil(() => Input.GetMouseButtonUp(0));
			if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonUp(0))
			{
				StopAllCoroutines();
				questCanvas.enabled = false;
			}
		}

	}
	public void NextNode(string fieldName)
	{
		if (_parser != null)
		{
			StopCoroutine(_parser);
			_parser = null;
		}
		foreach (NodePort p in currentGraph.current.Ports)
		{
			if (p.fieldName == fieldName)
			{
				currentGraph.current = p.Connection.node as BaseNode;
				break;
			}

		}
		_parser = StartCoroutine(ParseNode());
	}

	public IEnumerator TypeText(string textToType, TextMeshProUGUI textLabel)
	{
		textLabel.text = string.Empty;
		float t = 0;
		int charIndex = 0;
		while (charIndex < textToType.Length)
		{
			t += Time.deltaTime * typeWriterSpeed;
			charIndex = Mathf.FloorToInt(t);
			charIndex = Mathf.Clamp(charIndex, 0, textToType.Length);
			textLabel.text = textToType.Substring(0, charIndex);
			yield return null;
		}
		textLabel.text = textToType;
	}

	public void ChangeQuest(int questNumber)
	{
		switch (questNumber)
		{
			case 0:
				currentGraph = graphs[0];
				break;
			case 1:
				currentGraph = graphs[1];
				break;
			case 2:
				currentGraph = graphs[2];
				break;
			case 3:
				currentGraph = graphs[3];
				break;
			case 4:
				currentGraph = graphs[4];
				break;
			case 5:
				currentGraph = graphs[5];
				break;
			case 6:
				currentGraph = graphs[6];
				break;
			default:
				break;
		}
	}
}
