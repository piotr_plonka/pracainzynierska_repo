using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GirlHouseQuest : MonoBehaviour
{
	public QuestNodeParser questNodeParser;
	[Header("Error Popup")]
	public GameObject ErrorMassage;
	[Header("Needed Item")]
	public InventoryObject inventory;

	[Header("New Quest")]
	public int questNumber;
	
	public void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player")
		{
			CheckForItem();
		}
	}
	public void CheckForItem()
	{
		bool stop = false;
		if (GetComponentInChildren<QuestNodeParser>().questItem == null)
		{
			GetComponentInChildren<QuestNodeParser>().ChangeQuest(questNumber);
			GetComponentInChildren<QuestNodeParser>().StartDialogue();
		}
		else
		{
			for (int i = 0; i < inventory.GetSlots.Length; i++)
			{
				if (inventory.GetSlots[i].item.Id == GetComponentInChildren<QuestNodeParser>().questItem.data.Id)
				{
					questNumber++;
					GetComponentInChildren<QuestNodeParser>().ChangeQuest(questNumber);
					GetComponentInChildren<QuestNodeParser>().StartDialogue();
					break;
				}
				
			}
			if (stop == false)
			{
				StartCoroutine(ShowMessage(2f));
			}
		}
	}
	IEnumerator ShowMessage(float delay)
	{
		ErrorMassage.SetActive(true);
		yield return new WaitForSeconds(delay);
		ErrorMassage.SetActive(false);
	}
}
