using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHurtPlayer : MonoBehaviour
{
	private float waitToHurt = 2f;
	private bool isTouching;
	[SerializeField]
	private int damageToGive = 10;
	void Update()
	{
		if (isTouching)
		{
			waitToHurt -= Time.deltaTime;
			if(waitToHurt <= 0)
			{
				FindObjectOfType<Player>().TakeDamage(damageToGive);
				waitToHurt = 2f;
			}
		}
	}
	private void OnCollisionEnter2D(Collision2D other)
	{
		if (other.collider.tag == "Player")
			other.gameObject.GetComponent<Player>().TakeDamage(damageToGive);
	}
	private void OnCollisionStay2D(Collision2D other)
	{
		if (other.collider.tag == "Player")
			isTouching = true;

	}
	private void OnCollisionExit2D(Collision2D other)
	{
		if (other.collider.tag == "Player")
		{
			waitToHurt = 2f;
			isTouching = false;
		}
	}
}
