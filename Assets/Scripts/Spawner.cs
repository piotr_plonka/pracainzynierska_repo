using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    public GameObject monster;
    public bool respawn;
    public float spawnDelay;
    public float currentTime;
    public bool spawning;


    void Start()
    {
        Spawn();
        currentTime = spawnDelay;
    }
    void Update()
    {
        if (spawning)
        {
            currentTime -= Time.deltaTime;
            if (currentTime <= 0)
            {
                Spawn();
            }
        }
    }
    public void Respawn()
    {
        spawning = true;
        currentTime = spawnDelay;
    }
    void Spawn()
    {
        IEnemy instance = Instantiate(monster, transform.position, Quaternion.identity).GetComponent<IEnemy>();
        instance.Spawner = this;
        spawning = false;
    }
}