using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;

public class Description : MonoBehaviour
{
    public Text nameText, rarityText, descriptionText, buffsText;
    public static Description Instance;

	private void Start()
    {
        if (Instance == null)
            Instance = this;
        else
        {
            Debug.LogError("Error: There are multiple descriptions present, deleting old one");
            Destroy(Instance);
            Instance = this;
        }
    }

    private void OnDisable()
    {
        Instance = null;
    }

    public void AssignValues(string _name, string _rarity, string _description, string _buffs)
    {
        nameText.text = _name;
        rarityText.text = _rarity;
        descriptionText.text = _description;
        buffsText.text = _buffs;
    }


}