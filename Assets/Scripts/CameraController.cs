using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target;
    public float smoothing;
    public Vector2 minPos;
    public Vector2 maxPos;
    //public BoxCollider2D boundBox;
    //public Vector2 minBounds;
    //public Vector2 maxBounds;
    //private Camera theCamera;
    //private float halfHeight;
    //private float halfWidth;

    void Start()
    {
        //minBounds = boundBox.bounds.min;
        //maxBounds = boundBox.bounds.max;
        //theCamera = GetComponent<Camera>();
        //halfHeight = theCamera.orthographicSize;
        //halfWidth = halfHeight * Screen.width / Screen.height;
    }

    // Update is called once per frame
    void Update()
    {
        //transform.position = new Vector3(target.transform.position.x, target.transform.position.y, transform.position.z);

        //float clampedX = Mathf.Clamp(transform.position.x, minBounds.x + halfWidth, maxBounds.x - halfWidth);
        //float clampedY = Mathf.Clamp(transform.position.y, minBounds.y + halfHeight, maxBounds.y - halfHeight);

        //transform.position = new Vector3(clampedX, clampedY, transform.position.z);
    }
	private void LateUpdate()
	{
		if (transform.position != target.position)
		{
            Vector3 targetPosition = new Vector3(target.transform.position.x, target.transform.position.y, transform.position.z);
            targetPosition.x = Mathf.Clamp(targetPosition.x, minPos.x, maxPos.x);
            targetPosition.y = Mathf.Clamp(targetPosition.y, minPos.y, maxPos.y);
            transform.position = Vector3.Lerp(transform.position, targetPosition, smoothing) ;
		}
	}
}
