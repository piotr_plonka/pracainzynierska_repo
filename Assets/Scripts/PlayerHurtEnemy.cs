using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHurtEnemy : MonoBehaviour
{
	public int damageToGive = 2;
	private void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag == "Enemy")
		{
			EnemyAISlime eHealthManSlime;
			EnemyAILog eHeathManLog;
			eHealthManSlime = other.gameObject.GetComponent<EnemyAISlime>();
			eHeathManLog = other.gameObject.GetComponent<EnemyAILog>();
			eHealthManSlime.HurtEnemy(damageToGive);
			eHeathManLog.HurtEnemy(damageToGive);
		}
	}
}
