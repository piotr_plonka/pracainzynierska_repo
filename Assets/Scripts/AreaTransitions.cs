using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaTransitions : MonoBehaviour
{
    private CameraController cameracon;
    public Vector2 newMinCamPos;
    public Vector2 newMaxCamPos;
    public GameObject newCameraTranCol;
    public GameObject oldCameraTranCol;

    void Start()
    {
        cameracon = Camera.main.GetComponent<CameraController>();
    }

	private void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag == "Player")
		{
            cameracon.minPos = newMinCamPos;
            cameracon.maxPos = newMaxCamPos;
            oldCameraTranCol.SetActive(false);
            newCameraTranCol.SetActive(true);
        }
	}

	
}
