﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

[NodeWidth(304)]
[NodeTint("#555555")]
[CreateNodeMenu("Quest/Quest Dialogue", 1)]
public class QuestDialogueNode : BaseNode {

	[Input] public int entry;
	[Output] public int exit;
	public string speakerName;
	[TextArea]
	public string dialogueLine;

	public override string GetString()
	{
		return "QuestDialogueNode/" + speakerName + "/" + dialogueLine;
	}
}