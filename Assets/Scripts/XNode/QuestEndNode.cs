﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;
[NodeWidth(304)]
[NodeTint("#595761")]
[CreateNodeMenu("Quest/Quest End Node", 4)]
public class QuestEndNode : BaseNode{

	[Input] public int entry;

	public string speakerName;
	[TextArea]
	public string endDiaglogue;

	public override string GetString()
	{
		return "End/" + speakerName + "/" + endDiaglogue;
	}

}