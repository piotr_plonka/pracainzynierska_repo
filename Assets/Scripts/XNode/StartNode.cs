﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

[NodeTint("#6B6460")]
[CreateNodeMenu("Base/Start Node", 1)]
public class StartNode : BaseNode {

	[Output] public int exit;

	public override string GetString()
	{
		return "Start";
	}
}