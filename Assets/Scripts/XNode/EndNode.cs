using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

[NodeTint("#595761")]
[CreateNodeMenu("Base/End Node", 3)]
public class EndNode : BaseNode
{
	[Input] public int entry;

	public string switchScene;

	public override string GetString()
	{

		return "End/" + switchScene;
	}
	
}
