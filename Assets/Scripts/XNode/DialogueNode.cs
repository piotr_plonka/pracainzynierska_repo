﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

[NodeWidth(304)]
[NodeTint("#555555")]

[CreateNodeMenu("Base/Dialogue Node", 2)]
public class DialogueNode : BaseNode {
	
	[Input] public int entry;
	[Output] public int exit;
	public string speakerName;
	[TextArea]
	public string dialogueLine;
	public Sprite sprite;

	public override string GetString()
	{
		return "DialogueNode/" + speakerName + "/" + dialogueLine;
	}

	public override Sprite GetSprite()
	{
		return sprite;
	}
}