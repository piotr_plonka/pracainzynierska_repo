﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

[NodeTint("#555555")]
[NodeWidth(304)]
[CreateNodeMenu("Quest/Quest Item", 2)]
public class QuestItemNode : BaseNode {

	[Input] public int entry;
	[Output] public int exit;

	public ItemObject questItem;

	public string speakerName;
	[TextArea]
	public string dialogueLine;

	public override string GetString()
	{
		return "QuestItemNode/" + speakerName + "/" + dialogueLine;
	}

	public override ItemObject GetItemObject()
	{
		return questItem;
	}
	

}