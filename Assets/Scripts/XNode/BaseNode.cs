﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;
using UnityEngine.SceneManagement;

[CreateNodeMenu("Base/Base Node", 4)]

public class BaseNode : Node {

	public virtual string GetString()
	{
		return null;
	}
	public virtual Sprite GetSprite()
	{
		return null;
	}
	public virtual ItemObject GetItemObject()
	{
		return null;
	}
	public virtual string GetMoney()
	{
		return null;
	}
	
}