using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;

public class DayNightScript : MonoBehaviour
{
    public Text timeDisplay;
    public Text dayDisplay;
    public Volume ppv;
    public float tick;
    public float seconds;
    public int mins;
    public int hours;
    public int days = 1;
    public bool activateLights; 
    public GameObject[] lights;
    public GameObject timeBackground;
    [Header("Gameobjects to spawn/delete")]
    public GameObject sellhouseGirl;

    void Start()
    {
        timeBackground.GetComponent<Image>().color = new Color((float)0.1, (float)0.7, 1, 1);
        ppv = gameObject.GetComponent<Volume>();

    }

    void FixedUpdate()
    {
        CalcTime();
        DisplayTime();

    }

    public void CalcTime()
    {
        seconds += Time.fixedDeltaTime * tick;

        if (seconds >= 60)
        {
            seconds = 0;
            mins += 1;
        }

        if (mins >= 60)
        {
            mins = 0;
            hours += 1;
        }

        if (hours >= 24)
        {
            hours = 0;
            days += 1;
        }
        ControlPPV();
    }

    public void ControlPPV()
    {
        if (hours >= 20 && hours < 21)
        {
            ppv.weight = (float)mins / 60;
            if (activateLights == false)
            {
                if (mins > 10)
                {
                    for (int i = 0; i < lights.Length; i++)
                    {
                        lights[i].SetActive(true);
                    }
                    activateLights = true;
                    
                }
            }
        }
        if(hours == 20)
		{
            timeBackground.GetComponent<Image>().color = Color.Lerp( new Color((float)0.1, 0, (float)0.5, 1), new Color((float)0.1, (float)0.7, 1, 1), 1 - (float)mins / 60);
            
        }


        if (hours >= 6 && hours < 7)
        {
            ppv.weight = 1 - (float)mins / 60;
            if (activateLights == true)
            {
                if (mins > 30)
                {
                    for (int i = 0; i < lights.Length; i++)
                    {
                        lights[i].SetActive(false);
                    }
                    activateLights = false;
                    
                }
            }
        }
        if (hours == 6)
		{
            timeBackground.GetComponent<Image>().color = Color.Lerp( new Color((float)0.1, (float)0.7, 1, 1), new Color((float)0.1, 0, (float)0.5, 1), 1 - (float)mins / 60);
        }
        if (hours == 8)
        {
            sellhouseGirl.SetActive(true);
        }
        if(hours == 18)
        {
            sellhouseGirl.SetActive(false);
        }
        
    }

    public void DisplayTime()
    {
        timeDisplay.text = string.Format("{0:00}:{1:00}", hours, mins);
        dayDisplay.text = "Dzie�: " + days;
    }

}